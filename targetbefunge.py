import os
from time import sleep
from pypy.rlib.jit import JitDriver, hint

def out(char):
    os.write(1, char)

class RAM(object):
    def __init__(self, data=None):
        self.data = [" " for i in range(80 * 25)]
        if data is not None:
            x, y = 0, 0
            for row in data.split("\n"):
                for col in row:
                    self.data[(y * 80 + x)] = col
                    x += 1
                x = 0
                y += 1

    def set(self, po, value):
        x, y = po.x, po.y

        self.data[(x % 80) + (y % 25) * 80] = value

    def get(self, po):
        data = ""

        x, y = po.x, po.y
        data = self.data[(x % 80) + (y % 25) * 80]

        assert isinstance(data, str)
        return data

    def to_str(self, ip=None):
        os.write(1, "\n\n")
        for y in range(0, 25):
            for x in range(0, 80):
                val = self.get(SimpleIP(x, y))
                if ip and (x, y) == (ip.x, ip.y):
                    os.write(1, "#")
                    os.write(1, val)
                else:
                    os.write(1, " ")
                    os.write(1, val)
            os.write(1, "\n")
        os.write(1, "\n")


class SimpleIP(object):
    def __init__(self, x, y):
        self.x, self.y = x, y

class IP(SimpleIP):
    def __init__(self):
        self.x, self.y = 0, 0
        self.dir = ">"

    def advance(self):
        if self.dir == ">":
            self.x += 1
        elif self.dir == "<":
            self.x -= 1
        elif self.dir == "^":
            self.y -= 1
        elif self.dir == "v":
            self.y += 1

        self.x = self.x % 80
        self.y = self.y % 25

    def turn(self, dir):
        self.dir = dir

    def pos(self):
        return self.x, self.y

    #def random(self):
        #self.dir = random.choice("<>^v")


class Stack(object):
    def __init__(self):
        self.stack = []

    def push(self, data):
        self.stack.append(int(data))

    def pop(self):
        try:
            return (self.stack.pop(),)
        except IndexError:
            return (0, )

    def pop2(self):
        a = self.pop()
        b = self.pop()
        return b + a

    def pop3(self):
        a = self.pop()
        b = self.pop()
        c = self.pop()
        return c + b + a

_commands = {}
def command(name, args=2):
    def cmdfun(fun):
        def execution(stack):
            if args == 1:
                v = stack.pop()
            elif args == 2:
                v = stack.pop2()
            elif args == 3:
                v = stack.pop3()

            result = fun(*v)

            if isinstance(result, tuple):
                stack.push(result[0])
                if len(result) > 1:
                    stack.push(result[1])
            elif result is not None:
                stack.push(result)

        _commands[name] = execution
        return execution

    return cmdfun

@command("+")
def add(a, b):
    return a + b

@command("-")
def sub(a, b):
    return a - b

@command("*")
def mul(a, b):
    return a * b

@command("/")
def div(a, b):
    assert b != 0
    return a / b

@command("%")
def mod(a, b):
    assert b != 0
    return a % b

@command("\\")
def swap(a, b):
    return b, a

@command(":", 1)
def duplicate(a):
    return a, a

@command(".", 1)
def out_num(a):
    out(str(a) + " ")

@command(",", 1)
def out_ascii(a):
    out(chr(a))

@command("!", 1)
def not_(a):
    return 0 if a != 0 else 1

@command("`")
def gt(a, b):
    return 1 if a > b else 0

def get_location(ip):
    return "%s_%s" % (ip.x, ip.y)
jitdriver = JitDriver(greens=['ip'],
        reds=['running', 'code', 'stack', 'ram'],
        get_printable_location=get_location)

def mainloop(ram):
    running = True
    ip = IP()
    stack = Stack()
    code = " "

    while running:
        jitdriver.jit_merge_point(ip=ip, running=running, ram=ram, code=code, stack=stack)
        code = ram.get(ip)
        if code in "0123456789":
            stack.push(int(code))
        elif code in _commands:
            _commands[code](stack)
        elif code in "<>^v":
            ip.turn(code)
        elif code == '"':
            stringmode = True
            while stringmode:
                ip.advance()
                char = ram.get(ip)
                if char == '"':
                    stringmode = False
                else:
                    stack.push(ord(char))

        elif code in "|_":
            datum = stack.pop()[0]
            iszero = int(datum == 0)
            lr = int(code == "|")
            dir = "<^>v"[iszero * 2 + lr]
            ip.turn(dir)

        elif code == "p":
            x, y = stack.pop2()
            v = stack.pop()[0]
            ram.set(SimpleIP(x, y), chr(v))
        elif code == "g":
            x, y = stack.pop2()
            val = ord(ram.get(SimpleIP(x, y)))
            stack.push(val)

        elif code == "$":
            stack.pop()
        elif code == "#":
            ip.advance()
        elif code == "@":
            running = False
        elif code == " ":
            pass
        else:
            ram.to_str(ip)
            sleep(1)

        ip.advance()

    ram.to_str(ip)

def entry_point(argv):
    if len(argv) == 1:
        mainloop(RAM("""\
>25*"!dlrow ,olleH":v
                 v:,_@
                 >  ^"""))
    else:
        fd = os.open(argv[1], os.O_RDONLY, 0)
        data = os.read(fd, 80 * 25)
        os.close(fd)
        mainloop(RAM(data))
    return 0

def jitpolicy(driver):
    from pypy.jit.codewriter.policy import JitPolicy
    return JitPolicy()

def target(*args):
    return entry_point,None

if __name__ == "__main__":
    import sys
    if len(sys.argv)==1:
        mainloop(RAM("""\
>25*"!dlrow ,olleH":v
                 v:,_@
                 >  ^"""))
    else:
        fd = os.open(sys.argv[1], os.O_RDONLY, 0)
        data = os.read(fd, 80 * 25)
        os.close(fd)
        mainloop(RAM(data))
